## TP2 : Ansible

1. Vérifier l’installation d’Ansible sur votre poste de travail en utilisant le commande : `$ ansible --version`.

2. Créer le fichier hosts permettant de contacter les quatre serveurs que vous avez créés dans le TP précédent. Aidez-vous de la documentation officielle : [Ansible inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).

3. Vérifiez que pouvez contacter les serveurs en utilisant Ansible, pour ce faire, vous pouvez vous servir de la commande ping intégrée à Ansible.

4. Créer un playbook Ansible, veuillez lire la documentation : [Ansible playbook](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html).

5. En utilisant votre playbook, installez docker et le firewall de votre choix.

6. Configurez votre firewall pour qu’il n’autorise pas les connexions uniquement sur les ports des services : HTTP, HTTP et SSH.

7. Démarrer un serveur web sur le por HTTP en utilisant docker et ansible.

Le playbook pour cet exercice est dans le fichier playbook.yml.

### Application web

Le playbook web.yml déploie 2 conteneurs sur 2 instances différentes, un conteneur postgresql et un conteneur python qui installe et fais fonctionner une Application Django minimale qui communique avec le conteneur de base de données.

Ce playbook fait appel à un autre fichier de tâches qui Configure le pare-feu.
Ensuite il lance un playbook pour la base de données et un autre pour l'application web en elle même.
