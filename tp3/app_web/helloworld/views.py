from django.shortcuts import render

from helloworld.forms import MessageForm
from helloworld.models import Message

def message_view(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save()
    form = MessageForm()
    return render(request, 'helloworld/message_list.html', context={'form': form, 'object_list': Message.objects.all()})