terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "keypair" {
  name       = "ordi-perso"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCuns6sT2ec7zBAPpc+v3VELY/XSquw1LOi7ec/qpgwWq5SSRoV5AK46l1cWhKFQ+C1Bf0YiRse/BuSxFkndq7W8A0cMwIoCCeej83d/03jBf6CZT4J63t9QXDWpo/iubpdGx94hqOGsWB5Jk4ETdFr+b6YzhRwp2mTLbf+4a0ZMCVZbzVGv5Ml7ZjiJmHsUgP3czZjcr9cPVWMqIjXavxU7u3Y/nzKq1xjHDFmXW1FA5u7HZx08IdrAXr/wAzKvARgbwD999KMyzUIM6XuLeBMzfMDWvwXWHIQP67ZGZ/dem5ib72aWBvtJfO7TViG+3oVNsfy67I02UehLkbv91MWJnE6Pd820/lnBSlGhCQFLEOAAxL7TlSb5XfN5S5sl+0+I/oYjcABSUOm02tutQrZkas3simvHpfOm+JAc5NNlcYczxiMrs1mVOGlnKS+BGTPDY12Edp+D9DDQw2g9tnU+DEkVQYIl0UQIQVO/zi5BbRQvPxktMxxUbmQ7byIzUqMRmPTMlgVVfti6fqe74SacbVwXXz/vJpaUsKazEoWyhUR7tv+rqfmiwB3OYij7oYU3PcA1cYP/HmJJKpvOmMKW71zGVfp3SBRnOF5W0zO1hziP2MGS/jcXFpySKMRPYWUej8fDkw3vsez6+6jHYbJZSrbtN8E4u7MUsT+FmxOWw== benjamin.lemaire59850@gmail.com"
}

resource "openstack_compute_instance_v2" "terraform_instances_ubuntu" {
    count = 4
    name = "terraform-test-${count.index}"
    provider = openstack
    image_name = "ubuntu-20.04"
    flavor_name = "normale"
    key_pair = "${openstack_compute_keypair_v2.keypair.name}"
}